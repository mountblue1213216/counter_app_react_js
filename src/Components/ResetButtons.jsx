import React from "react";
import { BiRefresh, BiReset } from "react-icons/bi";

const ResetButtons = ({ resetCounters, resetCart }) => {
  return (
    <div>
      <button onClick={resetCounters}>
        <BiRefresh />
      </button>{" "}
      <button onClick={resetCart}>
        <BiReset />
      </button>{" "}
    </div>
  );
};

export default ResetButtons;
