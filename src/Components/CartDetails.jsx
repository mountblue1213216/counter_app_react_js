import React from "react";
import { BsFillCartFill } from "react-icons/bs";

const CartDetails = ({ countersInCart }) => {
  return (
    <div>
      <BsFillCartFill />
      {countersInCart}
    </div>
  );
};

export default CartDetails;
